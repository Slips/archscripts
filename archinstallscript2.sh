#!/bin/sh

# HELLO
#
# Read this script in it's entirety before executing it.
# DO NOT EXECUTE SCRIPTS WITHOUT READING THEM FIRST.
#
# If you are not okay with any action taken by this script, press N at the next prompt, whether
# or not your drives are properly formatted.
#
# Exit this prompt by pressing q.
#
# -Slips

cat ./archinstallscript2.sh | less
echo "Continue? (Y/N)"
read continue
while :
do
	case $continue in
		[yY][eE][sS]|[yY])
			echo "Okay..."
			break
			;;
		[nN][oO]|[nN])
			echo "Stopping..."
			exit 1
			;;
		*)
			echo "Not understood."
			;;
	esac
done

ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime

sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf
echo Enter Hostname:
read hostname
echo $hostname > /etc/hostname

echo 127.0.0.1	        localhost >> /etc/hosts
echo ::1		localhost >> /etc/hosts
echo 127.0.1.1	$hostname.localdomain	$hostname >> /etc/hosts

mkinitcpio -P
echo Set Root Password:
passwd 

if [ $(which sudo) != "sudo not found" ]; then
        echo "Sudo found, editing config..."
        sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
fi

if [ $(which doas) != "doas not found" ]; then
        echo "Doas found, making config..."
        echo "permit persist :wheel" > /etc/doas.conf
fi

echo "Setting up non-root user, please enter username:"
read username
useradd -m -G wheel -s /bin/$(cat /shellchoice) $username
echo "Set Non-root password:"
passwd $username
			
echo "Would you like to install grub for UEFI(1) or BIOS(2)?"
while :
do
        read grubTarget
        case $grubTarget in
                [uU][eE][fF][iI]|[1])
                        echo "UEFI selected."
                        grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch
                        break
                        ;;
                [bB][iI][oO][sS]|[2])
                        echo "BIOS selected."
                        while :
                        do
                                echo "Please give drive path (i.e /dev/___)"
                                read drivePath
				echo "Is $drivePath correct? Yes/No (Case Sensitive):"
                                read driveIsCorrect
                                if [ $driveIsCorrect = "Yes" ]; then                                      
                                        echo "Okay, continuing..."                                        
                                        break                                                             
                                elif [ $driveIsCorrect = "No" ]; then                                     
                                        echo "Returning to drive path selection..."                       
                                else                                                                      
                                        echo "Input not understood, returning, please type "Yes" or "No" (case sensitive) only."                                                                                    
                                fi                                                                        
                        done                                                                              
                        grub-install --recheck $drivePath                                                 
                        break                                                                             
                        ;;                                                                                
                *)                                                                                        
                        echo "Input not understood, please try again:"                                    
                        ;;                                                                                
        esac                                                                                              
done
grub-mkconfig -o /boot/grub/grub.cfg
rm /shellchoice
echo "Install phase two complete. To finish, exit this chroot with the exit command, umount -R /mnt, and reboot!"
