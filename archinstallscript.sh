#!/bin/sh

# HELLO
#
# Read this script in it's entirety before executing it.
# DO NOT EXECUTE SCRIPTS WITHOUT READING THEM FIRST.
#
# If you are not okay with any action taken by this script, press N at the next prompt, whether
# or not your drives are properly formatted.
#
# Exit this prompt by pressing q.
#
# -Slips

cat ./archinstallscript.sh | less
echo "CONFIRM THAT YOU HAVE MOUNTED YOUR MAIN PARTITION TO /mnt AND YOUR SWAP WITH swapon (Y/N):"
while :
do

	read drivesformatted
	case $drivesformatted in
		[nN][oO]|[nN])
			echo "PLEASE MOUNT DRIVES: mount /dev/root_partition /mnt && swapon /dev/swap_partition"
			exit 1
			;;
		[yY][eE][sS]|[yY])
			echo "Continuing..."
			break
			;;
		*)
			echo "Input not understood. Try again."
			;;
	esac
done

while :
do
        echo "What shell would you like to install?"
        read shellchoice
        echo "Is $shellchoice your selection?"
        read confirmation
        case $confirmation in
                [yY][eE][sS]|[yY])
                        echo "Okay, locking in $shellchoice..."
                        shellchoice=$(echo $shellchoice | tr [:upper:] [:lower:])
			echo $shellchoice > /mnt/shellchoice
                        break
                        ;;
                [nN][oO]|[nN])
                        echo "Okay, please make another selection."
                        ;;
                [*])
                        echo "Input not understood, presuming no..."
                        ;;
        esac
done

echo "Would you like sudo (1), doas (2), or both (3)?"
while :
do
        read rootTool
        case $rootTool in
                [sS][uU][dD][oO]|[1])
			pacstrap /mnt base linux linux-firmware base-devel sudo networkmanager $shellchoice neovim grub
                        break
                        ;;
                [dD][oO][aA][sS]|[2])
			pacstrap /mnt base linux linux-firmware base-devel doas networkmanager $shellchoice neovim grub
                        break
                        ;;
                [bB][oO][tT][hH]|[3])
			pacstrap /mnt base linux linux-firmware base-devel sudo doas networkmanager $shellchoice neovim grub
                        break
                        ;;
                *)
                        echo "Input not understood, please input one of the two choices or it's corresponding number."
                        ;;
        esac
done

genfstab -U /mnt >> /mnt/etc/fstab
cp ./archinstallscript2.sh /mnt

echo "=================================================="
echo "||STAGE ONE COMPLETE"
echo "||From here:"
echo "||arch-chroot /mnt, execute archinstallscript2.sh."
echo "=================================================="
